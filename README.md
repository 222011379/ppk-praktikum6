## PPK Praktikum 6

#Nama   : Maulana Pandudinata<br>
#NIM    : 222011379<br>
#Kelas  : 3SI3<br>

## Video Praktikum<br>
![](Dokumentasi/ppk-praktikum6.mp4)<br>


## Tampilan Google Cloud Service<br>
![](Dokumentasi/11.png)<br>

## Credential<br>
![](Dokumentasi/22.png)<br>

## Oauth Consent Screen<br>
![](Dokumentasi/33.png)<br>

## Database<br>
![](Dokumentasi/44.png)<br>

## Login<br>
![](Dokumentasi/55.png)<br>

## Register<br>
![](Dokumentasi/66.png)<br>

## Login with google<br>
![](Dokumentasi/77.png)<br>

## Foto<br>
![](Dokumentasi/88.png)<br>







