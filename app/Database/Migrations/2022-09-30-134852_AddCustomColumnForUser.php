<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class AddCustomColumnForUser extends Migration
{
    public function up()
    {
        $fields = [
            "gender SET('male', 'female') NULL DEFAULT NULL COMMENT 'gender user'",
            'first_name' => [
                'type' => 'text',
                'constraint' => '50',
                'null' => true,
            ],
            'last_name' => [
                'type' => 'VARCHAR',
                'constraint' => '50',
                'null' => true,
            ],
            'avatar' => [
                'type' => 'VARCHAR',
                'constraint' => '1000',
                'null' => true,
            ],
            'oauth_id' => [
                'type' => 'VARCHAR',
                'constraint' => '1000',
                'null' => true,
            ],
            'phone_number' => [
                'type' => 'VARCHAR',
                'constraint' => '20',
                'null' => true,
            ],
            'description' => [
                'type' => 'VARCHAR',
                'constraint' => '1200',
                'null' => true,
            ],
            'full_address' => [
                'type' => 'VARCHAR',
                'constraint' => '400',
                'null' => true,
            ],

        ];

        $this->forge->addColumn('users', $fields);
    }

    public function down()
    {
        $fields = [
            'gender',
            'first_name',
            'last_name',
            'phone_number',
            'description',
            'full_address',

        ];
        $this->forge->dropColumn('users', $fields);
    }
}
