<?php

namespace App\Models;

use CodeIgniter\Model;
use CodeIgniter\Shield\Models\UserModel as ModelsUserModel;

class UserModel extends ModelsUserModel
{
    protected $table          = 'users';
    protected $primaryKey     = 'id';
    protected $allowedFields = [
        'username',
        'status',
        'status_message',
        'active',
        'last_active',
        'deleted_at',
        'gender',
        'first_name',
        'last_name',
        'avatar',
        'phone_number',
        'full_address',
        'oauth_id',
        'created_at',
        'updated_at',
    ];

    function isAlreadyRegister($authid){
		return $this->db->table('users')->getWhere(['oauth_id'=>$authid])->getRowArray()>0?true:false;
	}
	function updateUserData($userdata, $authid){
		$this->db->table("users")->where(['oauth_id'=>$authid])->update($userdata);
	}
	function insertUserData($userdata){
		$this->db->table("users")->insert($userdata);
	}
    function getUserData($email){
        return $this->db->table('users')->getWhere(['email'=>$email])->getRowArray();
    }
}
