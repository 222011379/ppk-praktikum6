<?= $this->extend('layouts/template') ?>

<?= $this->section('content'); ?>
<section class="vh-100" style="background-color: #44cef6;">
  <div class="container py-5 h-100">
    <div class="row d-flex justify-content-center align-items-center h-100">
      <div class="col col-xl-10">
        <div class="card" style="border-radius: 1rem;">
                <?php if (session('error') !== null): ?>
                    <div class="alert alert-danger" role="alert"><?=session('error')?></div>
                <?php elseif (session('errors') !== null): ?>
                    <div class="alert alert-danger" role="alert">
                        <?php if (is_array(session('errors'))): ?>
                            <?php foreach (session('errors') as $error): ?>
                                <?=$error?>
                                <br>
                            <?php endforeach?>
                        <?php else: ?>
                            <?=session('errors')?>
                        <?php endif?>
                    </div>
                <?php endif?>
                <?php if (session('message') !== null): ?>
                <div class="alert alert-success" role="alert"><?=session('message')?></div>
                <?php endif?>
          <div class="row g-0">
            <div class="col-md-6 col-lg-5 d-none d-md-block">
              <img src="https://mdbcdn.b-cdn.net/img/Photos/new-templates/bootstrap-login-form/img3.webp"
                alt="login form" class="img-fluid" style="border-radius: 1rem 0 0 1rem;" />
            </div>
            <div class="col-md-6 col-lg-7 d-flex align-items-center">
              <div class="card-body p-4 p-lg-5 text-black">

                <form action="<?=url_to('login')?>" method="POST">
                  <?=csrf_field()?>

                  <div class="d-flex align-items-center mb-3 pb-1">
                    <i class="fas fa-cubes fa-2x me-3" style="color: #ff6219;"></i>
                    <span class="h1 fw-bold mb-0">Logo</span>
                  </div>

                  <h5 class="fw-normal mb-3 pb-3" style="letter-spacing: 1px;">Sign into your account</h5>

                  <div class="form-outline mb-4">
                    <input type="email" id="email" class="form-control form-control-lg" name="email" value="<?=old('email')?>" name="email" required/>
                    <label class="form-label" for="email">Email address</label>
                  </div>

                  <div class="form-outline mb-4">
                    <input type="password" id="password" class="form-control form-control-lg" name="password" required/>
                    <label class="form-label" for="password">Password</label>
                  </div>

                  <div class="pt-1 mb-4">
                    <button class="btn btn-dark btn-lg btn-block" type="submit">Login</button>
                  </div>
                  <?php if (setting('Auth.allowMagicLinkLogins')): ?>
                    <a class="small text-muted" href="<?=url_to('magic-link')?>">Forgot password?</a>
                  <?php endif?>
                  <p class="mb-5 pb-lg-2" style="color: #393f81;">Don't have an account? <a href="/register"
                      style="color: #393f81;">Register here</a></p>
                  <?= $google_button ?>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<?= $this->endSection(); ?>