<?=$this->extend('layouts/template');?>

<?=$this->section('content');?>
<section>
<div class="cover-container d-flex h-100 p-3 mx-auto flex-column text-center">
    <main role="main" class="inner cover">
        <h1 class="cover-heading">About this page</h1>
        <p class="lead">
            Lorem ipsum dolor sit, amet consectetur adipisicing elit. Tempora animi maiores porro molestiae, sit hic saepe suscipit? Ullam explicabo voluptas quidem magni culpa nihil libero voluptates placeat quo recusandae? Enim.
        </p>
    </main>
</div>
</section>

<?=$this->endSection();?>



